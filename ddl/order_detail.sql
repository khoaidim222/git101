CREATE OR REPLACE VIEW base.ORDER_DETAIL AS 
  with order_agg as (
    SELECT
        TRUNC(ORDER_PURCHASE_TIMESTAMP,
        'MONTH')           AS ORDER_MONTH,
        o.ORDER_ID
        , o.CUSTOMER_ID
        , count(OI.PRODUCT_ID) NBR_OF_ITEMS
    from DEMO_ORDER o
        LEFT JOIN DEMO_ORDER_ITEM oi on o.ORDER_ID = oi.ORDER_ID
    where TRUNC(ORDER_PURCHASE_TIMESTAMP,
        'MONTH') >= '01-JAN-17'
    group by o.ORDER_ID, o.CUSTOMER_ID, TRUNC(ORDER_PURCHASE_TIMESTAMP,
        'MONTH')
)
select
    order_month
    , o.ORDER_ID
    , o.CUSTOMER_ID
    , o.NBR_OF_ITEMS
    , PAYMENT_VALUE
from order_agg o
    left join DEMO_PAYMENT p on o.order_id = p.ORDER_ID;
